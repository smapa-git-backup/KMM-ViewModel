pluginManagement {
    repositories {
        gradlePluginPortal()
        mavenCentral()
        google()
    }
}

rootProject.name = "kmm-viewmodel"

include(":kmm-viewmodel-compose")
include(":kmm-viewmodel-core")
